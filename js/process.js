function showAlert()
{
    var title = document.getElementById("ttle").value;
    var rating = document.getElementById("rating").value;
    var genre = document.getElementById("genre").value;
    var year = document.getElementById("year").value;
    var link = document.getElementById("link").value;


    if(!title || 0 === title.length){

        alert('title cant be empty');
        return;

    }

        if(!rating || 0 === rating.length){

        alert('rating cant be empty');
        return;

    }

    alert(
        "Title: "+title+
        "\nRating: "+rating+
        "\nGenre: "+genre+
        "\nYear: "+year+
        "\nLink: "+link
    );
}